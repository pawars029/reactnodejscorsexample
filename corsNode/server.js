const express = require('express')
const app = express()

const PORT = 3001;

app.all('/re1', function (req, res) {
    res.send('Hello from Endpoint1!')
})

app.all('/req2', function (req, res) {
    res.header("Access-Control-Allow-Origin", "http://localhost:3000");
    res.header("Access-Control-Allow-Headers");
    res.send('Allowed origin')
})

app.get('/', function (req, res) {
    res.send('Cors example')
})

app.listen(PORT, function () {
    console.log('Server running on port '+PORT)
})